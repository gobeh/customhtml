package com.example.customlisthtml.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(schema = "public", name = "category")
public class Category {
    @Id
    @GeneratedValue
    @Column(name = "id", length = 4)
    Long id;

    @Column(name = "nama")
    String nama;

/*    @OneToMany(
            mappedBy = "category",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Article> articles = new ArrayList<>();

    //Constructors, getters and setters removed for brevity

    public void addCategory(Article article) {
        articles.add(article);
        article.setCategory(this);
    }

    public void removeCategory(Article article) {
        articles.remove(article);
        article.setCategory(null);
    }*/

}
