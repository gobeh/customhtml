package com.example.customlisthtml.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "public", name = "article")
public class Article {

    @Id
    @GeneratedValue
    @Column(name = "id", length = 4)
    Long id;

    @Column(name = "title")
    String title;

    @Column(name = "content")
    String content;

    @ManyToOne
    @JoinColumn(name = "id_category")
    private Category category;


}
