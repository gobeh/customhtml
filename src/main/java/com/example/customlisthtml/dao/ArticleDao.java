package com.example.customlisthtml.dao;

import com.example.customlisthtml.entity.Article;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ArticleDao extends JpaRepository<Article, Long> {
    List<Article> findTop5ByCategoryIdOrderByIdDesc(Long id);
}
