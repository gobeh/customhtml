package com.example.customlisthtml.dao;

import com.example.customlisthtml.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryDao extends JpaRepository<Category, Long> {
}
