package com.example.customlisthtml.dto;

import com.example.customlisthtml.entity.Article;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoryDto {
    Long id;
    String nama;
    List<Article> articleList = new LinkedList<>();
}
