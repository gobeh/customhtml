package com.example.customlisthtml;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomlisthtmlApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomlisthtmlApplication.class, args);
	}

}
