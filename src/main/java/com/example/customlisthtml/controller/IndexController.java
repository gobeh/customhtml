package com.example.customlisthtml.controller;

import com.example.customlisthtml.dao.ArticleDao;
import com.example.customlisthtml.dao.CategoryDao;
import com.example.customlisthtml.dto.CategoryDto;
import com.example.customlisthtml.entity.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.LinkedList;
import java.util.List;

@Controller
public class IndexController {

    @Autowired
    private CategoryDao categoryDao;

    @Autowired
    private ArticleDao articleDao;

    @GetMapping(value = {"/", "/index"})
    public String getIndexPage(ModelMap mm) {

        List<CategoryDto> categoryDtoList = new LinkedList<>();

        for(Category cat : categoryDao.findAll()){
            CategoryDto categoryDto = new CategoryDto();

            categoryDto.setId(cat.getId());
            categoryDto.setNama(cat.getNama());
            categoryDto.setArticleList(articleDao.findTop5ByCategoryIdOrderByIdDesc(cat.getId()));
            categoryDtoList.add(categoryDto);
        }

        mm.addAttribute("listCategories", categoryDtoList);

        return "index";
    }
}


