package com.example.customlisthtml.controller;

import com.example.customlisthtml.dao.ArticleDao;
import com.example.customlisthtml.dao.CategoryDao;
import com.example.customlisthtml.dto.ArticleDto;
import com.example.customlisthtml.entity.Article;
import com.example.customlisthtml.entity.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class ApiController {

    @Autowired
    private CategoryDao categoryDao;

    @Autowired
    private ArticleDao articleDao;

    @PostMapping("api/category")
    public Category createCategory(@RequestBody Category category) {

        Category oCat = new Category();
        oCat.setNama(category.getNama());
        categoryDao.save(oCat);
        return oCat;
    }

    @PostMapping("api/article")
    public Article createArticle(@RequestBody ArticleDto article) {

        Article oArt = new Article();

        Optional<Category> category = categoryDao.findById(article.getCategory());
        oArt.setCategory(category.get());
        oArt.setTitle(article.getTitle());
        oArt.setContent(article.getContent());
        articleDao.save(oArt);
        return oArt;
    }
}
